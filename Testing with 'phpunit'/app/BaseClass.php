<?php

namespace App;

class BaseClass
{
    public static function postTestInfo()
    {
        $person = new Person("FirstName","LastName", date_create('01-01-2000'));
        return $person;
    }

    public static function getPersonInfo(Person $person) : string
    {
        return $person->getFirstName() . " " . $person->getLastName() . " " . date_format($person->getDayOfBirth(), 'd-m-Y');
    }
}