<?php
namespace App;

use  PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $person;
    private $firstName = "FirstName";
    private $lastName = "LastName";
    private $dayOfBirth = '01-01-2000';

    protected function setUp(): void
    {
        $this->person = new Person($this->firstName, $this->lastName, date_create($this->dayOfBirth));
    }

    /**
     * @dataProvider userProvider1
     */
    public function testFirstName(string $fName)
    {
        $this->assertSame($fName, $this->person->getFirstName());
        $this->assertEquals($fName,$this->person->getFirstName());
    }
    /**
     * @dataProvider userProvider2
     */
    public function testLastName(string $lName)
    {
        $this->assertSame($lName, $this->person->getLastName());
        $this->assertEquals($lName,$this->person->getLastName());
    }
    /**
     * @dataProvider userProvider3
     */
    public function testDayOfBirth(string $date)
    {
        $this->assertEquals(date_create($date), $this->person->getDayOfBirth());
    }

    public function testPostTestInfo()
    {
        $this->assertInstanceOf(Person::class, BaseClass::postTestInfo());
    }

    public function testGetPersonInfo()
    {
        $this->expectOutputString(BaseClass::getPersonInfo($this->person));
        print("FirstName LastName 01-01-2000");
    }

    public function userProvider1()
    {
        return[
            ["FirstName"],
        ];
    }

    public function userProvider2()
    {
        return[
            ["LastName"],
        ];
    }

    public function userProvider3()
    {
        return[
            ['01-01-2000'],
        ];
    }
}