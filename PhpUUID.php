<?php
require 'vendor\autoload.php';

use Ramsey\Uuid\Uuid;

 // Версия 1 (основана на времени)
$uuid1 = Uuid::uuid1();
echo $uuid1->toString() . "<br>";

// Версия 3. (на базе названия и MD5)
$uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, 'php.net');
echo $uuid3->toString() . "<br>";

// Версия 4 (случайный набор символов)
$uuid4 = Uuid::uuid4();
echo $uuid4->toString() . "<br>";

// Версия 5 (на базе названия и SHA1)
$uuid5 = Uuid::uuid5(Uuid::NAMESPACE_DNS, 'php.net');
echo $uuid5->toString() . "<br>";
