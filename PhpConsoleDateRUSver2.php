<?php

$_monthsList = array("January" => "Январь", "February" => "Февраль",
    "March" => "Март", "April" => "Апрель", "May" => "Май", "June" => "Июнь",
    "July" => "Июль", "August" => "Август", "September" => "Сентябрь",
    "October" => "Октябрь", "November" => "Ноябрь", "December" => "Декабрь");

$_daysList = array("Monday" => "Понедельник", "Tuesday" => "Вторник",
    "Wednesday" => "Среда", "Thursday" => "Четверг", "Friday" => "Пятница", "Saturday" => "Суббота",
    "Sunday" => "Воскресенье");

$currentDate = date("d, l, F, Y, H:i");
$currentDate = str_replace(date("F"), " ".$_monthsList[date("F")]." ", $currentDate);
echo $currentDate = str_replace(date("l"), " ".$_daysList[date("l")]." ", $currentDate);